# import log_helper
import logging.config
import logging
import pathlib
import sys

logger = logging.getLogger(__name__)


def sub_fcn():
    log_helper.fcn('entry')
    magic_number = 42
    logging.debug('debug from sub')
    log_helper.fcn()
    logging.info('info from sub')
    logging.warning('warning from sub')
    log_helper.fcn(f'magic! {magic_number}')
    logging.error('error from sub')
    logging.critical('critical from sub')
    log_helper.fcn('exit')


if __name__ == "__main__":
    parent_dir = pathlib.Path(__file__).absolute().parent.parent.absolute()
    sys.path.insert(0, str(parent_dir))
    import log_helper

    conf_file = parent_dir / 'log_helper' / 'logging.conf'
    logging.config.fileConfig(conf_file, disable_existing_loggers=False)
    log_helper.set_log_level_for_all_handlers(logging.INFO)
    sub_fcn()
else:
    import log_helper
