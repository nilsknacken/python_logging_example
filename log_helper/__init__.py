import logging
from inspect import currentframe
from os.path import basename
from inspect import getframeinfo, stack


def fcn(message=""):
    "Automatically log the current function details."
    caller = getframeinfo(stack()[1][0])
    logging.debug("%s::%s:%d - %s" % (basename(caller.filename), caller.function, caller.lineno, message))


def set_log_level_for_all_handlers(log_level=logging.DEBUG):
    for handler in logging.getLogger().handlers:
        handler.setLevel(log_level)
