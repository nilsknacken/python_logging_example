import logging
import logging.config
import logging.handlers
import log_helper
import folder.subfile as sub

logging.config.fileConfig('log_helper/logging.conf', disable_existing_loggers=False)
logger = logging.getLogger(__name__)


def main():
    log_helper.fcn("testing from main")
    logging.debug('debug log')
    sub.sub_fcn()


def set_log_level(log_level=logging.DEBUG):
    for handler in logging.getLogger().handlers:
        handler.setLevel(log_level)


# set_log_level_to_debug()
# log_helper.set_log_level_for_all_handlers(logging.DEBUG)
main()
